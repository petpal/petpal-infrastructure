resource "aws_s3_bucket" "static_readonly" {
  count = var.SHOULD_ENABLE ? 1 : 0
  bucket = var.domain_name
  acl = "public-read"

  versioning {
    enabled = true
  }

  tags = {
    Name = "petpal-static-readonly"
  }

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::petpal.com.cy/*"
        }
    ]
}
EOF

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  force_destroy     = true
}

