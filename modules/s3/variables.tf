variable "SHOULD_ENABLE" {
  type        = bool
}

variable domain_name {
  description = "This should be the same name as the name if the bucket is going to be used to serve a static website"
}
