
output "bucket_zone_id" {
  value = aws_s3_bucket.static_readonly[0].hosted_zone_id
}

output "bucket_domain_name" {
  value = aws_s3_bucket.static_readonly[0].website_domain
}
