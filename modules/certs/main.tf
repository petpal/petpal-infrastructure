
resource "aws_acm_certificate" "acm_certificate" {
  domain_name               = var.domain_name
  validation_method         = "DNS"

  tags = {
    Environment = "prod"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "acm_certificate_wildcard" {
  domain_name               = "*.${var.domain_name}"
  validation_method         = "DNS"

  tags = {
    Environment = "wildcard"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "validation_route53_record" {
  name    = aws_acm_certificate.acm_certificate.domain_validation_options[0].resource_record_name
  type    = aws_acm_certificate.acm_certificate.domain_validation_options[0].resource_record_type
  zone_id = var.zone_id
  records = [aws_acm_certificate.acm_certificate.domain_validation_options[0].resource_record_value]
  ttl     = "60"
}

resource "aws_route53_record" "validation_route53_record_wildcard" {
  name    = aws_acm_certificate.acm_certificate_wildcard.domain_validation_options[0].resource_record_name
  type    = aws_acm_certificate.acm_certificate_wildcard.domain_validation_options[0].resource_record_type
  zone_id = var.zone_id
  records = [aws_acm_certificate.acm_certificate_wildcard.domain_validation_options[0].resource_record_value]
  ttl     = "60"
}

resource "aws_acm_certificate_validation" "acm_certificate_validation" {
  certificate_arn         = aws_acm_certificate.acm_certificate.arn
  validation_record_fqdns = aws_route53_record.validation_route53_record.*.fqdn

}

resource "aws_acm_certificate_validation" "acm_certificate_validation_dev" {
  certificate_arn         = aws_acm_certificate.acm_certificate_wildcard.arn
  validation_record_fqdns = aws_route53_record.validation_route53_record_wildcard.*.fqdn

}
