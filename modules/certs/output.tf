output "cert_arn" {
  value = aws_acm_certificate.acm_certificate.arn
}

output "cert_arn_wildcard" {
  value = aws_acm_certificate.acm_certificate_wildcard.arn
}
