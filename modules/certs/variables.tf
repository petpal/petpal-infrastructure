variable "domain_name" {
  description = "The domain name for which the certificate will be issued"
  type        = string
}

variable "zone_id" {
  description = "The zone_id in which the certificate will apply"
  type        = string
}
