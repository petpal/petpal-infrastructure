output "public_subnet_id" {
  value = aws_subnet.eu-west-1a-public[0].id
}

output "private_subnet_id" {
  value = aws_subnet.eu-west-1a-private[0].id
}

output "vpc_id" {
  value = aws_vpc.default[0].id
}

