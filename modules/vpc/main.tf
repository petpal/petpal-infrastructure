resource "aws_vpc" "default" {
  count                 = var.SHOULD_ENABLE ? 1 : 0
  cidr_block            = var.vpc_cidr
  enable_dns_hostnames  = true

  tags = {
    Name = "petpal-aws-vpc"
  }
}

resource "aws_internet_gateway" "default" {
  count  = var.SHOULD_ENABLE ? 1 : 0
  vpc_id = aws_vpc.default[0].id
}

resource "aws_security_group" "nat" {
  count       = var.SHOULD_ENABLE ? 1 : 0
  description = "Allow traffic to pass from the private subnet to the internet"
  name        = "vpc_nat"
  vpc_id      = aws_vpc.default[0].id

  tags = {
    Name = "petpal-nat-sg"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.private_subnet_cidr]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.private_subnet_cidr]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "nat" {
  count                       = var.SHOULD_ENABLE ? 1 : 0
  ami                         = "ami-0cf654585ca8229c6"
  availability_zone           = var.availability_zone
  instance_type               = var.nat_instance_type
  vpc_security_group_ids      = [aws_security_group.nat[0].id]
  subnet_id                   = aws_subnet.eu-west-1a-public[0].id
  associate_public_ip_address = true
  source_dest_check           = false

  tags = {
    Name = "NAT instance"
  }
}

resource "aws_eip" "nat" {
  count    = var.SHOULD_ENABLE ? 1 : 0
  instance = aws_instance.nat[0].id
}

// ################### Public Subnet #############################
resource "aws_subnet" "eu-west-1a-public" {
  count             = var.SHOULD_ENABLE ? 1 : 0
  vpc_id            = aws_vpc.default[0].id
  cidr_block        = var.public_subnet_cidr
  availability_zone = var.availability_zone

  tags = {
    Name = "Public Subnet"
  }
}

resource "aws_route_table" "eu-west-1a-public" {
  count  = var.SHOULD_ENABLE ? 1 : 0
  vpc_id = aws_vpc.default[0].id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default[0].id
  }

  tags = {
    Name = "Public Subnet"
  }
}

resource "aws_route_table_association" "eu-west-1a-public" {
  count           = var.SHOULD_ENABLE ? 1 : 0
  subnet_id       = aws_subnet.eu-west-1a-public[0].id
  route_table_id  = aws_route_table.eu-west-1a-public[0].id
}

// ##############################################################
// ##################### Private Subnet #########################
resource "aws_subnet" "eu-west-1a-private" {
  count  = var.SHOULD_ENABLE ? 1 : 0
  vpc_id = aws_vpc.default[0].id
  cidr_block = var.private_subnet_cidr
  availability_zone = var.availability_zone

  tags = {
    Name = "Private Subnet"
  }
}

resource "aws_route_table" "eu-west-1a-private" {
  count  = var.SHOULD_ENABLE ? 1 : 0
  vpc_id = aws_vpc.default[0].id

  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = aws_instance.nat[0].id
  }

  tags = {
    Name = "Private Subnet"
  }
}

resource "aws_route_table_association" "eu-west-1b-private" {
  count           = var.SHOULD_ENABLE ? 1 : 0
  subnet_id       = aws_subnet.eu-west-1a-private[0].id
  route_table_id  = aws_route_table.eu-west-1a-private[0].id

}
// ##############################################################


