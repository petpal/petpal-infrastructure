variable "SHOULD_ENABLE" {
  description = "Set to true to create the bastion host"
  type        = bool
}

variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default     = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  description = "CIDR for the Public Subnet"
  default     = "10.0.0.0/24"
}

variable "private_subnet_cidr" {
  description = "CIDR for the Private Subnet"
  default     = "10.0.1.0/24"
}

variable "nat_instance_type" {
  description = "The size of the nat aws instance that will be created"
  default     = "t2.micro"
}

variable "availability_zone" {
  description = "The availability zone within which all of the VPC-related resources will be created"
  default     = "eu-west-1a"
}
