variable "SHOULD_ENABLE" {
  description = "Set to true to create the webserver cluster"
  type        = bool
}

variable "certificate_id" {
  description = "The ARN/ID of the certificate that will be registered with load-balancer"
  type        = string
}

variable "instance_type" {
  description = "The type of instance that will be used for the launch configuration"
  default     = "t2.micro"
}

variable "petpal_repository_url" {
  description = "The URL for the project's repository"
  type        = string
  default     = "https://github.com/MrfksIv/petpal.git"
}

variable "cluster_name" {
  description = "The name of the cluster (e.g. dev/prod/etc.)"
  type        = string
}

variable "admin_ssh_key_filename" {
  description = "The name of the public.key file of the admin"
  type        = string
}

variable "private_subnet_id" {
  description = "The id of the Private Subnet created by the VPC module"
  type        = string
}

variable "vpc_id" {
  description = "The VPC id within all subnets are created"
  type        = "string"
}

variable "public_subnet_id" {
  description = "The id of the Public Subnet created by the VPC module"
  type        = string
}

variable "max_size" {
  description = "The maximum number of instances that the load-balancer will be able to deploy"
  type        = number
}

variable "min_size" {
  description = "The minimum number of instances that the load-balancer will be able to deploy"
  type        = number
}

variable "server_port" {
  description = "The number of the port on which the node.js server is running on in the EC2 instances"
  type        = number
}
