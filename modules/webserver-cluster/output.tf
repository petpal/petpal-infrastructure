output "elb_zone_id" {
  description = "The zone of the elastic load balancer to point the route53 record at"
  value       = aws_elb.webserver_loadbalancer[0].zone_id
}

output "elb_dns_name" {
  description = "The DNS name of the elastic load balancer to point the route53 record at"
  value       = aws_elb.webserver_loadbalancer[0].dns_name
}
