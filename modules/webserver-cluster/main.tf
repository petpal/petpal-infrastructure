

// The first step in creating a webserver cluster, is to create a Launch Configuration.
// This specifies how each EC2 instance in the Autoscaling Group (ASG) will be setup.
resource "aws_launch_configuration" "webserver_launch_config" {
  count           = var.SHOULD_ENABLE ? 1 : 0
  image_id        = "ami-06358f49b5839867c"
  instance_type   = var.instance_type

  security_groups = [aws_security_group.webserver_secgroup[0].id]

  user_data       = <<-EOF
                    #!/bin/bash
                    sudo apt-get install -y git
                    cd /home/ubuntu
                    git clone "${var.petpal_repository_url}"
                    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
                    sudo apt install -y nodejs
                    source /home/ubuntu/.bashrc
                    sudo /usr/bin/npm install -g typescript
                    cd petpal
                    sudo /usr/bin/npm install
                    sudo /usr/bin/npm audit fix
                    sudo /usr/bin/npm run build
                    cd server
                    sudo /usr/bin/tsc
                    /usr/bin/node main.js
                    EOF

  key_name       = aws_key_pair.admin_key_launch_conf[0].key_name

  // this tells terraform the order of instance creation/deletion
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_key_pair" "admin_key_launch_conf" {
  count       = var.SHOULD_ENABLE ? 1 : 0
  key_name    = "admin_key_launch_conf"
  public_key  = file(var.admin_ssh_key_filename)
}

data "aws_availability_zones" "all" {}

resource "aws_elb" "webserver_loadbalancer" {
  count               = var.SHOULD_ENABLE ? 1 : 0

  name                = "${var.cluster_name}-server-asg-elb"
  security_groups     = [aws_security_group.elb_secgroup[0].id]
  subnets             = [var.public_subnet_id]

  listener {
    instance_port = var.server_port
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }

  listener {
    instance_port       = var.server_port
    instance_protocol   = "http"
    lb_port             = 443
    lb_protocol         = "https"
    ssl_certificate_id  = var.certificate_id
  }

  health_check {
    healthy_threshold = 2
    interval = 30
    target = "HTTP:${var.server_port}/elb-health-check"
    timeout = 3
    unhealthy_threshold = 2
  }
}

// After creating the Launch Configuration above, we can go ahead and create the
// Autoscaling Group (ASG):
resource "aws_autoscaling_group" "webserver_asg" {
  launch_configuration  = aws_launch_configuration.webserver_launch_config[0].id
  availability_zones    = data.aws_availability_zones.all.names

  vpc_zone_identifier = [var.private_subnet_id]

  min_size              = var.SHOULD_ENABLE ? var.min_size : 0
  max_size              = var.SHOULD_ENABLE ? var.max_size : 0

  load_balancers        = [aws_elb.webserver_loadbalancer[0].name] // tells the ASG to register each instance to the CLB
  health_check_type     = "ELB" // changes the default 'EC2' health-check to the more robust load-balancer health-check

  tag {
    propagate_at_launch = true
    key                 = "Name"
    value               = "webserver-asg"
  }
}


resource "aws_security_group" "webserver_secgroup" {
  count   = var.SHOULD_ENABLE ? 1 : 0
  name    = "${var.cluster_name}-ec2-webserver"
  vpc_id  = var.vpc_id

  ingress {
    from_port   = var.server_port
    to_port     = var.server_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  # We permit outbound traffic to everywhere and allow all protocols and ports.
  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

// Because CLBs do not allow any traffic by default, we need to create a security group that allows traffic:
resource "aws_security_group" "elb_secgroup" {
  count   = var.SHOULD_ENABLE ? 1 : 0
  name    = "${var.cluster_name}-elb-secgroup"
  vpc_id  = var.vpc_id

  # Allow all outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow incoming traffic on port 80 from everywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

