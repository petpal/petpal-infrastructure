variable "server_name" {
  description = "The name of the server (prod/dev)"
  type        = string
}

variable "admin_ssh_key_filename" {
  description = "The name of the public.key file of the admin"
  type        = string
}

variable "subnet_id" {
  description = "The subnet id in which the bastion instance will be placed"
  type        = string
}

variable "vpc_id" {
  description = "The VPC id within all subnets are created"
  type        = "string"
}

variable "SHOULD_ENABLE" {
  description = "Set to true to create the bastion host"
  type        = bool
}
