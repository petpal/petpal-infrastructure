
resource "aws_instance" "bastion" {
  count                       = var.SHOULD_ENABLE ? 1 : 0
  key_name                    = aws_key_pair.admin_key[0].key_name
  ami                         = "ami-03746875d916becc0"
  instance_type               = "t2.micro"
  associate_public_ip_address = true

  vpc_security_group_ids      = [aws_security_group.bastion-sg[0].id]
  subnet_id                   = var.subnet_id

  tags = {
    Name = "Bastion Host"
  }

  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update && sudo apt-get upgrade
              sudo apt-get install -y git ansible tree
              EOF
}

resource "aws_key_pair" "admin_key" {
  count       = var.SHOULD_ENABLE ? 1 : 0
  key_name    = "admin_key"
  public_key  = file(var.admin_ssh_key_filename)
}

resource "aws_security_group" "bastion-sg" {
  count   = var.SHOULD_ENABLE ? 1 : 0
  name    = "${var.server_name}-bastion-security-group"
  vpc_id  = var.vpc_id

  tags = {
    Name = "Bastion SecGroup"
  }
  # We allow inbound traffic only on port 22 using the TCP protocol.
  # Traffic is allowed to originate from any IP.
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  # We permit outbound traffic to everywhere and allow all protocols and ports.
  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}




