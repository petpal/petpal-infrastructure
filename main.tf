provider "aws" {
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "petpal"
  region                  = var.region
}

# We will store the terraform state file on S3 instead of storing it locally.
resource "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-state-petpal"
  region = var.region

  # Enable versioning so we can see the full version history of the state files
  versioning {
    enabled = true
  }

  # This will prevent the s3 bucket containing the state file from being deleted when `terraform destroy` is called
  lifecycle {
    prevent_destroy = true

  }

  # Enable server-side encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

# S3 on its own does not solve the biggest problem which is two different people
# running terraform apply at the (almost) the same time, thus before the state file
# has reached its final state. This may result in inconsistencies.
# To enable 'locking' we will use the DynamoDB AWS service
resource "aws_dynamodb_table" "terraform_state_locks" {
  name          = "terraform-state-locks"
  hash_key      = "LockID"
  billing_mode  = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }
}






