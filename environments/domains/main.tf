resource "aws_route53_zone" "prod" {
  name = var.prod_domain_name
}

resource "aws_route53_zone" "dev" {
  name = var.dev_domain_name
}

resource "aws_route53_record" "alias_route53_record" {
  zone_id = aws_route53_zone.prod.zone_id
  name    = aws_route53_zone.prod.name
  type    = "A"

  alias {
    name                   = var.cloudfront_domain_name
    zone_id                = var.cloudfront_hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ns_to_subdomain_record" {
  zone_id = aws_route53_zone.prod.zone_id
  name    = aws_route53_zone.dev.name
  type    = "NS"
  ttl     = 300

  records = aws_route53_zone.dev.name_servers
}

resource "aws_route53_record" "alias_route53_record_dev" {
  zone_id = aws_route53_zone.dev.zone_id
  name    = aws_route53_zone.dev.name
  type    = "A"

  alias {
    name                   = var.elb_dns_dev_name
    zone_id                = var.elb_zone_dev_id
    evaluate_target_health = true
  }
}


