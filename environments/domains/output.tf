output "route_53_prod_zone_id" {
  value = aws_route53_zone.prod.zone_id
}

output "route_53_dev_zone_id" {
  value = aws_route53_zone.dev.zone_id
}

output "route_53_prod_zone_name" {
  value = aws_route53_zone.prod.name
}

output "route_53_dev_zone_name" {
  value = aws_route53_zone.dev.name
}


