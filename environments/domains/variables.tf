variable "elb_zone_prod_id" {
  description = "The zone of the elastic load balancer to point the route53 record at"
  type        = string
}

variable "prod_domain_name" {
  description = "The production server's domain that will be created as a Route53 Hosted Zone"
  type        = string
}

variable "dev_domain_name" {
  description = "The development server's subdomain that will be created as a Route53 Hosted Zone"
  type        = string
}

variable "elb_dns_prod_name" {
  description = "The DNS name of the elastic load balancer to point the route53 record at"
  type        = string
}

variable "elb_zone_dev_id" {
  description = "The zone of the elastic load balancer to point the route53 record at"
  type        = string
}

variable "elb_dns_dev_name" {
  description = "The DNS name of the elastic load balancer to point the route53 record at"
  type        = string
}

variable "cloudfront_hosted_zone_id" {
  type        = string
}

variable "cloudfront_domain_name" {
  type        = string
}
