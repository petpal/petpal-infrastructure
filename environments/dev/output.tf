output "elb_zone_dev_id" {
  value = module.webserver.elb_zone_id
}

output "elb_dns_dev_name" {
  value = module.webserver.elb_dns_name
}

// TODO::::::: THE FOLLLOWING TWO SHOULD MOVE TO THE PRODUCTION SERVER AS SOON AS IT IS SETUP!!
output "elb_zone_prod_id" {
  value = module.webserver.elb_zone_id
}

output "elb_dns_prod_name" {
  value = module.webserver.elb_dns_name
}
