variable "region" {
  description = "The AWS region in which resources will be instantiated"
  default     = "eu-west-1"
}

variable "admin_ssh_key_filename" {
  description = "The name of the public.key file of the admin"
  default     = "~/.ssh/petpal_admin_id_rsa.pub"
}

variable "server_name" {
  description = "The type of server (dev / prod)"
  default     = "dev"
}
