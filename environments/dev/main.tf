provider "aws" {
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "petpal"
  region                  = "eu-west-1"
}

module "vpc" {
  SHOULD_ENABLE           = true
  source                  = "../../modules/vpc"
}

module "bastion" {
  SHOULD_ENABLE           = true
  source                  = "../../modules/bastion"
  admin_ssh_key_filename  = "~/.ssh/petpal_admin_id_rsa.pub"
  server_name             = "dev"
  subnet_id               = module.vpc.public_subnet_id
  vpc_id                  = module.vpc.vpc_id
}

module "s3" {
  SHOULD_ENABLE           = true
  source                  = "../../modules/s3"
  domain_name             = "petpal.com.cy"
}

module "certs" {
  source                  = "../../modules/certs"
  domain_name             = "petpal.com.cy"
  zone_id                 = module.domains.route_53_prod_zone_id
}

module "domains" {
  source                    = "../domains"

  elb_dns_prod_name         = module.webserver.elb_dns_name
  elb_zone_prod_id          = module.webserver.elb_zone_id
  elb_dns_dev_name          = module.webserver.elb_dns_name
  elb_zone_dev_id           = module.webserver.elb_zone_id

  prod_domain_name          = "petpal.com.cy"
  dev_domain_name           = "dev.petpal.com.cy"
  cloudfront_domain_name    = module.s3.bucket_domain_name
  cloudfront_hosted_zone_id = module.s3.bucket_zone_id
}


module "webserver" {
  SHOULD_ENABLE           = true
  source                  = "../../modules/webserver-cluster"
  admin_ssh_key_filename  = "~/.ssh/petpal_admin_id_rsa.pub"
  cluster_name            = "dev"
  vpc_id                  = module.vpc.vpc_id
  private_subnet_id       = module.vpc.private_subnet_id
  public_subnet_id        = module.vpc.public_subnet_id
  server_port             = 3001
  max_size                = 2
  min_size                = 1
  certificate_id          = module.certs.cert_arn_wildcard
}

output "bastion_ip" {
  value                   = module.bastion.bastion_public_ip
}

# At this point we have everything we need to store the terraform state file on S3
# however, it is still stored locally. We need to tell Terraform to change its backend
# to S3 rather than 'local':
# NOTE::: THE FIRST TIME, COMMENT OUT THE BACKEND CODE BELOW, SO THAT THE S3 BUCKET IS CREATED FIRST USING A LOCAL BACKEND.
# OTHERWISE, TERRAFORM THROWS AN ERROR THAT IT CAN NOT FIND THE S3 BUCKET TO STORE THE STATEFILE!!!
terraform {
  backend "s3" {
    bucket                  = "terraform-state-petpal"
    key                     = "global/s3/terraform.tfstate"
    region                  = "eu-west-1"
    profile                 = "petpal"

    dynamodb_table          = "terraform-state-locks"
    encrypt                 = true
    skip_region_validation  = true
  }
}
