variable "region" {
  description = "The region that the resources will be instantiated in"
  default = "eu-west-1"
}

